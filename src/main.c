#define _DEFAULT_SOURCE

#include <assert.h>

#include "mem_internals.h"
#include "mem.h"

#define BIG_CAPACITY 5000

#define START_TEST(test) \
printf(" The test has started\"%s\"...", #test); \
heap_init(0); \
printf("%s \n", test()); \
heap_term();

static struct block_header* get_last(struct block_header* heap_start) {
    struct block_header* cur_elem = heap_start;
    while (cur_elem->next) {
        cur_elem = cur_elem->next;
    }
    return cur_elem;
}


static char* min_test() {
    uint8_t* contents = _malloc(0);
    struct block_header* header = block_get_header(contents);

    assert(header->capacity.bytes == BLOCK_MIN_CAPACITY);
    assert(header->next != NULL);
    assert(!header->is_free);
    assert(header == HEAP_START);

    return "YEP";
}

static char* single_test() {
    uint8_t* contents = _malloc(BLOCK_MIN_CAPACITY + 1);
    struct block_header* header = block_get_header(contents);

    assert(header->capacity.bytes == BLOCK_MIN_CAPACITY + 1);
    assert(header->next != NULL);
    assert(!header->is_free);
    assert(header == HEAP_START);

    return "YEP";
}

static char* single_free_test() {
    _malloc(0);
    uint8_t* malloc1 = _malloc(0);
    uint8_t* malloc2 = _malloc(0);


    struct block_header* block1 = block_get_header(malloc1);
    struct block_header* block2 = block_get_header(malloc2);

    _free(malloc2);

    assert(block2->is_free);
    assert(block1->next == block2);
    assert(block2->next == NULL);

    return "YEP";
}


static char* free_test() {
    _malloc(0);
    uint8_t* malloc1 = _malloc(0);
    uint8_t* malloc2 = _malloc(0);
    uint8_t* malloc3 = _malloc(0);


    struct block_header* block1 = block_get_header(malloc1);
    struct block_header* block2 = block_get_header(malloc2);
    struct block_header* block3 = block_get_header(malloc3);

    size_t start_capacity = block2->capacity.bytes;

    _free(malloc3);
    _free(malloc2);

    size_t end_capacity = block2->capacity.bytes;

    assert(block2->is_free);
    assert(block3->is_free);
    assert(block1->next == block2);
    assert(block2->next == NULL);
    assert(end_capacity > start_capacity);

    return "YEP";
}


static char* extend_old_test() {
    uint8_t* big1 = _malloc(BIG_CAPACITY);
    uint8_t* big2 = _malloc(BIG_CAPACITY);

    struct block_header* block1 = block_get_header(big1);
    struct block_header* block2 = block_get_header(big2);

    assert(block1->next == block2);
    assert((void*)(block1->contents + block1->capacity.bytes) == block2);

    return "YEP";
}

static char* extend_in_new_place_test() {
    void* gap = mmap(HEAP_START + REGION_MIN_SIZE, 10, 0, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    uint8_t* big1 = _malloc(capacity_from_size((block_size) { REGION_MIN_SIZE }).bytes);
    uint8_t* big2 = _malloc(BIG_CAPACITY);

    struct block_header* block1 = block_get_header(big1);
    struct block_header* block2 = block_get_header(big2);

    munmap(gap, 10);

    assert(block1->next == block2);
    assert((void*)(HEAP_START + REGION_MIN_SIZE) != block2);

    return 0;
}

int main() {
    START_TEST(min_test);
    START_TEST(single_test);
    START_TEST(single_free_test);
    START_TEST(free_test);
    START_TEST(extend_old_test);
    START_TEST(extend_in_new_place_test);
}
